often_used_currencies = ["EUR", "USD", "RUB", "CZK", "GBP", "AUD", "JPY", "CHF"]
currencies_list_addr = "https://free.currencyconverterapi.com/api/v6/currencies"
converting_request = "https://free.currencyconverterapi.com/api/v6/convert?q={0}_{1}&compact=y"
